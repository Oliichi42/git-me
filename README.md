# git-me

`git me` is a very simple identity management script for the popular distributed version control system `git`.

It is written in Python (3.6+) which is easy to install on Linux distributions, and often already installed.

## Motivations

When you have multiple online identities, it can get a bit confusing with `git` as you either have to remember to
specify your identity for every repo or use a global configuration and end up leaking the wrong identity
to the wrong repository.

With `git-me` fully installed, you will not be able to use identity-leaking commands such as `git commit`
unless you have an identity specified **in that repository's local `.git/config` file**.

Of course, `git-me` provides a quick way to configure that identity from a list of presets that you provide,
either in a TSV file or by using `git me add`.

#### Side-note: git and SSH for multiple accounts

(Side-note:) In order to use multiple GitLab/GitHub accounts with different SSH keys, you can use a `ssh-config` like the
following:

```
Host acc1.gitlab
        HostName gitlab.com
        RSAAuthentication Yes
        IdentityFile ~/.ssh/acc1_rsa

Host o42.gitlab
        HostName gitlab.com
        RSAAuthentication Yes
        IdentityFile ~/.ssh/oliichi42_rsa

Host gitlab.com
        HostName specify.a.gitlab.account
```

## Usage

`git me` is designed to be simple and intuitive. See some of the commands here:

### Managing identities

Identities will be stored in `~/.config/git-me` by default, but the `GIT_ME_STORE` environment variable can
be used to change that. The identity store file is a TSV (tab-seperated value) file.

#### To add an identity:

`git me add <slug> "<name>" <e-mail address>`

Note: a `slug` is an easy-to-type specifier that you choose; it must be unique.

Example:
`git me add personal "Joseph Watson" j.watson@example.com`


#### To list your identities:

`git me ls` will list your identities in a (currently skewed) table.

Example:
```
joe@jwpc:~$ git me ls
git-me identity store: /home/joe/.config/git-me
# Slug          Name            E-mail Address
personal                Joseph Watson           j.watson@example.com
```

#### To remove an identity:

`git me rm <slug>` will delete the identity with that slug.

Example:
`git me rm personal`

### Using an identity

`git me use <slug>` will configure the current git repo to use the identity with that slug.
Example:
```
joe@jwpc:~/gitrepo$ git config --list --local
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
joe@jwpc:~/gitrepo$ git me use personal
git-me identity store: /home/joe/.config/git-me
joe@jwpc:~/gitrepo$ git config --list --local
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
user.name=Joseph Watson
user.email=j.watson@example.com
```

### Checking that your identity is being used

You can use the command `git me assert` to assert that you have an identity set for the local repository.

It will warn you and fail if you do not.

The real power of this command comes from using the included `git-checked.py` wrapper as it will
automatically assert that you have a set identity BEFORE you commit.

You can use it by adding e.g. a bash alias.
```bash
alias git='python3.6 /path/to/git-checked.py'
```

## Installation

To install `git me` as a true and proper `git` subcommand, I would recommend saving the contents of this repository
somewhere and making `git-me.py` and `git-checked.py` executable.

Note: you may have to change the hashbang lines if your Python interpreter is not known as `python3.6`.

Then, you can create a symlink to make `git-me.py` usable as `git me`:

```bash
ln -s /path/to/git-me.py ~/.local/bin/git-me
```

Note: this assumes `~/.local/bin` is in your PATH. Adjust if necessary.

and add an alias to your `.bashrc` or other shell profile:
```bash
alias git='python3.6 /path/to/git-checked.py'
```

# Thanks

I wrote this tool for personal use but you are certainly free to use, modify or extend it.

Feel free to submit merge requests or issues.

Licence: MIT (as included).