import sys, subprocess, os

def eprint(*all):
    print(*all, file=sys.stderr)

if len(sys.argv) >= 2 and sys.argv[1] in ['commit', 'ci']:
    if os.system('git me assert') > 1:
        eprint("git-checked: Cancelling git action; fix git me issues first!")
        exit(1)
subprocess.run(['git'] + sys.argv[1:], stdout=sys.stdout, stderr=sys.stderr, stdin=sys.stdin, env=os.environ)